import 'package:flutter/material.dart';
import 'package:testme/models/Book.dart';
import 'package:testme/screens/BookDetailsPage.dart';

class BookWidget extends StatelessWidget {
  BookWidget(this.book);
  final Book book;
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(12),
      color: Colors.blue[900],
      margin: EdgeInsets.all(12),
      child: Column(
        children: [
          Expanded(
              child: Image.network(
            book.icon,
            fit: BoxFit.fitHeight,
          )),
          Text(
            book.title,
            style: TextStyle(color: Colors.white),
          ),
          Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BookDetailsPage(book.id.toString()),
                    ));
              },
              child: Text(
                "View",
                style: TextStyle(
                  color: Colors.blue[900],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
