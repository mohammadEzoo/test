import 'dart:convert';

import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:testme/models/Book.dart';

class Api {
  Api._();
  static String baseUrl = "https://talmihat.alkmal.com/api/v1/";
  static final Api apiClient = Api._();
  static final http.Client _httpClient = http.Client();

  Future<List<Book>> getAllBooks() async {
    try {
      final response = await _httpClient.get(
        baseUrl + "books",
      );

      if (response.statusCode == 200) {
        final json = jsonDecode(response.body);
        final BaseResponse baseResponse = BaseResponse.fromJson(json);

        if (baseResponse.success) {
          return baseResponse.books;
        } else {
          return Future.error("error ");
        }
      }
      return Future.error("check your internet connection");
    } on SocketException {
      //this in case internet problems
      return Future.error("check your internet connection");
    } on http.ClientException {
      //this in case internet problems
      return Future.error("check your internet connection");
    } catch (e) {
      print(e.toString());
      return Future.error(e.toString());
    }
  }

  Future<Book> getBook(String bookId) async {
    try {
      final response = await _httpClient.get(
        baseUrl + "books/" + bookId,
      );

      if (response.statusCode == 200) {
        final json = jsonDecode(response.body);
        final Book book = Book.fromJson(json["data"]);

        if (book != null) {
          return book;
        } else {
          return Future.error("error ");
        }
      }
      return Future.error("check your internet connection");
    } on SocketException {
      //this in case internet problems
      return Future.error("check your internet connection");
    } on http.ClientException {
      //this in case internet problems
      return Future.error("check your internet connection");
    } catch (e) {
      print(e.toString());
      return Future.error(e.toString());
    }
  }
}
