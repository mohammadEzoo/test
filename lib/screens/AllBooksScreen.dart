import 'package:flutter/material.dart';
import 'package:testme/Widgets/BookWidget.dart';
import 'package:testme/models/Book.dart';
import 'package:testme/services/Api.dart';

class AllBooksScreen extends StatefulWidget {
  AllBooksScreen({Key key}) : super(key: key);

  @override
  _AllBooksScreenState createState() => _AllBooksScreenState();
}

class _AllBooksScreenState extends State<AllBooksScreen> {
  List<Book> books;
  String error;
  getBooks() async {
    await Api.apiClient.getAllBooks().then((value) {
      setState(() {
        books = value;
      });
    }).catchError((onError) {
      setState(() {
        error = onError.toString();
      });
    });
  }

  @override
  void initState() {
    getBooks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Books"),
        backgroundColor: Colors.blue[900],
      ),
      body: CustomScrollView(
        slivers: [
          if (books == null)
            SliverToBoxAdapter(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          else if (error != null)
            SliverToBoxAdapter(
              child: Center(
                child: Text(error),
              ),
            )
          else
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 2.5 / 4, crossAxisCount: 2),
              delegate: SliverChildBuilderDelegate((contet, index) {
                return BookWidget(books[index]);
              }, childCount: books.length),
            )
        ],
      ),
    );
  }
}
