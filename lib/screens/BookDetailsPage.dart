import 'package:flutter/material.dart';
import 'package:testme/models/Book.dart';
import 'package:testme/services/Api.dart';
import 'package:url_launcher/url_launcher.dart';

class BookDetailsPage extends StatefulWidget {
  const BookDetailsPage(this.bookId);
  final String bookId;
  @override
  _BookDetailsPageState createState() => _BookDetailsPageState();
}

class _BookDetailsPageState extends State<BookDetailsPage> {
  Book book;
  String error;
  getBook() async {
    await Api.apiClient.getBook(widget.bookId).then((value) {
      setState(() {
        book = value;
      });
    }).catchError((onError) {
      setState(() {
        error = onError.toString();
      });
    });
  }

  @override
  void initState() {
    getBook();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (book == null)
            Center(
              child: CircularProgressIndicator(),
            )
          else if (error != null)
            Center(
              child: Text(error),
            )
          else
            Column(
              children: [
                Container(
                    height: sizeAware.height * 0.5,
                    width: sizeAware.width,
                    child: Image.network(
                      book.icon,
                      fit: BoxFit.fill,
                    )),
                InkWell(
                    child: Text(
                      book.slug,
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: sizeAware.width * 0.06,
                          fontWeight: FontWeight.bold),
                    ),
                    onTap: () async {
                      if (await canLaunch(book.link))
                        launch(book.link);
                      else
                        print('cant launch');
                    }),
              ],
            )
        ],
      ),
    );
  }
}
